variable "aws_region" {
  description = "The AWS region where resources will be deployed"
}

variable "access_key" {
  description = "The AWS access key for authentication"
  type        = string
  sensitive   = true
}


variable "secret_key" {
  description = "The AWS secret key for authentication"
  type        = string
  sensitive   = true
}

resource "aws_ecr_repository" "ecr" {
  name                 = "gitlab-ecr-tutorial"
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository_policy" "public_policy" {
  repository = aws_ecr_repository.ecr.name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "AllowPull"
        Effect = "Allow"
        Principal = "*"
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetRepositoryPolicy",
          "ecr:BatchGetImage"
        ]
      }
    ]
  })
}


