#!/bin/bash

set -e

cd "${DIRECTORY}" || exit 

# Correcting the package manager command
apt-get update && apt-get install -y curl unzip jq python3-pip
apt-get install -y python3.12-venv
python3 -m venv venv  # Corrected here
source venv/bin/activate
pip3 install awscli 

TERRAFORM_VERSION="1.0.5"
curl -o "terraform.zip" "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"
unzip terraform.zip && mv terraform /usr/local/bin/
rm terraform.zip

# ALL THE VARIABLES BELOW ARE DEFINED AS CICD VARIABLES IN THE GITLAB REPOSITORY OF THE PROJECT 
# DEFINE IN THE GITLAB VARIABLES  ==> settings => CICD => Variables
export access_key="${AWS_ACCESS_KEY}"
export secret_key="${AWS_SECRET_KEY}"
export aws_region="${AWS_REGION}"

terraform init 
terraform validate 
terraform plan -var "aws_region=${AWS_REGION}" -var "access_key=${AWS_ACCESS_KEY}" -var "secret_key=${AWS_SECRET_KEY}"
terraform apply -var "aws_region=${AWS_REGION}" -var "access_key=${AWS_ACCESS_KEY}" -var "secret_key=${AWS_SECRET_KEY}" -auto-approve
terraform destroy -var "aws_region=${AWS_REGION}" -var "access_key=${AWS_ACCESS_KEY}" -var "secret_key=${AWS_SECRET_KEY}" -auto-approve
